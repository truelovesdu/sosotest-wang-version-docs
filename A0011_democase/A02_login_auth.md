# 登录鉴权
登录鉴权是接口测试中几乎毕竟的一个场景，本示例主要讲解如何使用python模式封装登录关键字，直接将登录信息封装到头信息，用例可直接引用实现登录。<br>
同时，实现了如果检测到session中有登录信息时，不会重复登录，直接用当前session访问。解决了任务中多次重复登录问题，单个任务会话只需登录一次。


## step1：实现登录关键字
![图片](/image/LOGIN_02_intoAddLogin.png)
![图片](/image/LOGIN_01_addLoginPythonCode.png)
备注：context.current_session就是请求的session，使用此session执行请求，会自动带入上下文信息到用例中。

## step2: 在接口的准备中导入并调用登录函数
![图片](/image/LOGIN_03_调用login.png)

