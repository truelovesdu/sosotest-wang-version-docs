# 前后接口调用

在用例编写时，可能接口B要依赖接口A的返回值作为入参，此时可在接口B用例的准备中执行接口A，此时接口B就可以调用接口A产生的变量和上下文session。<br>
<br>
## 场景示例
要测试一个删除接口，需要先新增一条数据，拿到新增数据的主键，然后进行删除测试。<br>
例如我要测试删除缺陷，那么我需要新建一个缺陷，拿到缺陷id，然后再去删除缺陷。

### step1：新建缺陷接口
![图片](/image/接口用例调用_01_addbug.png)

### step2: 删除接口，调用step1的新建接口
![图片](/image/接口用例调用_02_delbugCallAddBug.png)




